# Bonsai-test


To start this in your minikube cluster just call 
`kubectl apply -f values.yaml`


Note: 3 node RabbitMQ cluster with persistence volumes attached to the storage. So even a StatefulSet scale to 0 the cluster will survive.



*  To horizontally scale this chart once it has been deployed

`replicas=3

rabbitmq.password="$RABBITMQ_PASSWORD"

rabbitmq.erlangCookie="$RABBITMQ_ERLANG_COOKIE"`










