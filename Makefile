

build:
	docker build -t bonsai-test/Dockerfile .

push:
	docker push bonsai-test/Dockerfile

deploy-kubernetes:
	kubectl apply -f values.yaml
